use movie_db

create view title_directors as 
select title,director_name
from movie, directors,movie_directors
where movies.movie_id=movie_directors.movie_id and movie_directors.director_id = directors.director_id

select * from titles_directors

create view usa_oscar_movies as
select title,oscars
from movies,countries,producer_countries
where movies.movie_id=producer_countries.movie_id and producer_countries.country_id=countries.country_id and country_name like "%usa%" and oscars >0

select * from usa_oscar_movies order by title;

call GetMovies();
call GetMovieByDirector("Francis Ford Coppola");
call GetMovieByCoStars("Orlando Bloom","Ian McKellen");